﻿using BSA_LINQ.Services;
using System;
using System.Linq;

namespace BSA_LINQ
{
    static class Menu
    {
        static LinqService _linqService;
        static bool isQuit = false;


        internal static void Start(LinqService linqService)
        {
            _linqService = linqService;
            GetMenu();
        }

        private static void GetMenu()
        {
            while (!isQuit) // the loop works until we select the action "0 - Quit"
            {
                ShowOptions(); // Displaying elements of necessary actions on the screen

                char choice = Console.ReadKey().KeyChar; // wait until the choice is made and write to the "choice" variable
                Console.WriteLine("\n");

                try
                {
                    switch (choice)
                    {
                        case '1':
                            Task1_GetProjectUserTasksCount();
                            break;
                        case '2':
                            Task2_GetUserTasks();
                            break;
                        case '3':
                            Task3_GetFinishedTasksForUser();
                            break;
                        case '4':
                            Task4_GetAgeLimitTeams();
                            break;
                        case '5':
                            Task5_GetSortedUsers();
                            break;
                        case '6':
                            Task6_GetUserLastProjectInfo();
                            break;
                        case '7':
                            Task7_GetProjectShortInfo();
                            break;
                        case '0':
                            isQuit = true;
                            break; // Quit from menu and programm
                        case 'Q':
                            isQuit = true;
                            break; // Quit from menu and programm
                        case 'q':
                            isQuit = true;
                            break; // Quit from menu and programm
                        default:
                            WriteText("Incorrect enter!", ConsoleColor.Red);
                            break;
                    }

                }
                catch (Exception e)
                {
                    WriteText(e.Message, ConsoleColor.Red);
                }
                finally
                {
                    WriteText("Press any key to continue...", ConsoleColor.Green);

                    Console.ReadKey();
                }
            }
        }

        private static void Task1_GetProjectUserTasksCount()
        {
            int id = GetId();

            var result = _linqService.Task1_GetProjectUserTasksCount(id);

            foreach (var pair in result)
            {
                Console.WriteLine($"Project ID: {pair.Key.Id} Number of tasks: {pair.Value}");
            }
            Console.WriteLine();

        }
        private static void Task2_GetUserTasks()
        {
            int id = GetId();
            var result = _linqService.Task2_GetUserTasks(id);

            if (result.Count() == 0)
                Console.WriteLine("No tasks that satisfy the condition were found");

            foreach (var task in result)
            {
                Console.WriteLine($"Task ID: {task.Id} Name: {task.Name} (length = {task.Name.Length}) Performer ID: {task.Performer.Id}");
            }
            Console.WriteLine();
        }
        private static void Task3_GetFinishedTasksForUser()
        {
            int id = GetId();
            var result = _linqService.Task3_GetFinishedTasksForUser(id);

            if (result.Count() == 0)
                Console.WriteLine("No tasks that satisfy the condition were found");

            foreach (var task in result)
            {
                Console.WriteLine($"Task Id: {task.id} Name: {task.name}");
            }

            Console.WriteLine();
        }

        private static void Task4_GetAgeLimitTeams()
        {
            var result = _linqService.Task4_GetAgeLimitTeams();
            foreach (var team in result)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Project ID: {team.id} Name: {team.name}");
                Console.ResetColor();
                Console.WriteLine("MEMBERS:");
                foreach (var member in team.members)
                {
                    Console.WriteLine($"Member ID: {member.Id}\tName: {member.FirstName} {member.LastName}\tRegisteredAt: {member.RegisteredAt}\tYear of birthday: {member.Birthday.Year}");
                }

                Console.WriteLine("=======================================");
            }
        }

        private static void Task5_GetSortedUsers()
        {
            var result = _linqService.Task5_GetSortedUsers();

            foreach (var user in result)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"User ID: {user.user.Id} Name: {user.user.FirstName}");
                Console.ResetColor();
                Console.WriteLine("TASKS:");

                foreach (var task in user.tasks)
                {
                    Console.WriteLine($"Task ID: {task.Id} Name: {task.Name}");
                }

                Console.WriteLine("=======================================");
            }
        }
        private static void Task6_GetUserLastProjectInfo()
        {
            int id = GetId();
            var userInfo = _linqService.Task6_GetUserLastProjectInfo(id);

            Console.WriteLine($"User ID: {userInfo.User.Id} " +
                $"Name: {userInfo.User.FirstName} {userInfo.User.LastName}");
            Console.WriteLine($"Number of tasks in last user project: {userInfo.TasksLastProject}");
            Console.WriteLine($"Number of not completed and canceled tasks: {userInfo.NotComletedTasks}");
            Console.WriteLine($"The longest task: TaskID: {userInfo.MaxTask?.Id.ToString() ?? "There are no tasks for this user"} Created: {userInfo.MaxTask?.CreatedAt.Date} Finished: {userInfo.MaxTask?.FinishedAt.Date}");
        }

        private static void Task7_GetProjectShortInfo()
        {
            var result = _linqService.Task7_GetProjectShortInfo();
            
            foreach (var p in result)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Project: {p.project.Id} {p.project.Name}");
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine($"The longest project task (by description):");
                Console.ResetColor();
                Console.WriteLine($"Task ID: {p.longDescriptionTask?.Id} Task description: {p.longDescriptionTask?.Description}");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine($"The shortest project task (by name):");
                Console.ResetColor();
                Console.WriteLine($"Task ID: {p.shortNameTask?.Id} Task name: {p.shortNameTask?.Name}");
                WriteText("Users in project team: ", ConsoleColor.Blue, false);
                Console.WriteLine($"{p.usersCount}");

                Console.WriteLine("=======================================");
            }

        }

        private static void ShowOptions()
        {
            Console.Clear();

            WriteText("╔═════════════════════════════════╗", ConsoleColor.Blue);
            WriteText("║ Welcome to Projects Repository! ║", ConsoleColor.Blue);
            WriteText("║ What do you want to do?         ║", ConsoleColor.Blue);
            WriteText("╚═════════════════════════════════╝", ConsoleColor.Blue);
            Console.WriteLine();


            WriteText("1", ConsoleColor.Green, false);
            Console.WriteLine(" - Отримати кiлькiсть таскiв у проектi конкретного користувача (по id)");

            WriteText("2", ConsoleColor.Green, false);
            Console.WriteLine(" - Отримати список таскiв, призначених для конкретного користувача (по id),\n    де name таска <45 символiв (колекцiя з таскiв)");

            WriteText("3", ConsoleColor.Green, false);
            Console.WriteLine(" - Отримати список (id, name) з колекцiї таскiв, якi виконанi (finished)\n    в поточному (2020) роцi для конкретного користувача (по id)");

            WriteText("4", ConsoleColor.Green, false);
            Console.WriteLine(" - Отримати список (id, iм'я команди i список користувачiв) з колекцiї команд,\n    учасники яких старшi 10 рокiв, вiдсортованих за датою реєстрацiї\n    користувача за спаданням, а також згрупованих по командах.");

            WriteText("5", ConsoleColor.Green, false);
            Console.WriteLine(" - Отримати список користувачiв за алфавiтом first_name (по зростанню)\n    з вiдсортованими tasks по довжинi name (за спаданням)");

            WriteText("6", ConsoleColor.Green, false);
            Console.WriteLine(" - Отримати задану структуру (передати Id користувача в параметри)");

            WriteText("7", ConsoleColor.Green, false);
            Console.WriteLine(" - Отримати таку структуру: Проект, Найдовший таск проекту (за описом),\n    Найкоротший таск проекту (по iменi), Загальна кiлькiсть користувачiв в\n    командi проекту, де або опис проекту >20 символiв, або кiлькiсть таскiв <3");

            WriteText("Q", ConsoleColor.Green, false);
            Console.WriteLine(" - Quit");

            Console.Write("\nSelect option: ");
        }

        private static void WriteText(string text, ConsoleColor color, bool isLine = true)
        {
            Console.ForegroundColor = color;

            if (isLine)
                Console.WriteLine(text);
            else
                Console.Write(text);

            Console.ResetColor();
        }

        private static int GetId(string str = "Enter User Id: ")
        {
            int inputText;
            do
            {
                Console.WriteLine(str);
            } while (!int.TryParse(Console.ReadLine(), out inputText));

            return inputText;
        }

    }
}
