﻿using System;

namespace BSA_LINQ.Entities
{
    public class TaskData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public int State { get; set; }
        public Project Project { get; set; }
        public UserData Performer { get; set; }
    }
}
