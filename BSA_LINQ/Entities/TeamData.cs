﻿using System;
using System.Collections.Generic;

namespace BSA_LINQ.Entities
{
    public class TeamData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<UserData> Members { get; set; }

    }
}
