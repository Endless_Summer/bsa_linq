﻿
namespace BSA_LINQ.Entities
{
    class UserInfo
    {
        public UserData User { get; set; }
        public ProjectData LastProject { get; set; }
        public int TasksLastProject { get; set; }
        public int NotComletedTasks { get; set; }
        public TaskData MaxTask { get; set; }
    }
}
