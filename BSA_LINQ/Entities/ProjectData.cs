﻿using System;
using System.Collections.Generic;

namespace BSA_LINQ.Entities
{
    public class ProjectData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public List<TaskData> Tasks { get; set; }
        public UserData Author { get; set; }
        public Team Team { get; set; }
    }
}
