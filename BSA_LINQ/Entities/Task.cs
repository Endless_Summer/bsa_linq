﻿using System;

namespace BSA_LINQ.Entities
{
    public class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public int State { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
    }
}
