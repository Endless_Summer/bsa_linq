﻿using BSA_LINQ.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace BSA_LINQ.Services
{
    internal class LinqService : IDisposable
    {
        readonly HttpClient client;
        List<User> users;
        List<Task> tasks;
        List<Team> teams;
        List<Project> projects;
        IEnumerable<ProjectData> projectStruct;

        public LinqService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("https://bsa20.azurewebsites.net");

            GetDataFromWebAPI();
            GetProjectStruct();
        }

        private void GetDataFromWebAPI()
        {
            // get all users
            string response = client.GetStringAsync("/api/Users").Result;
            users = JsonConvert.DeserializeObject<List<User>>(response);

            // get all tasks
            response = client.GetStringAsync("/api/Tasks").Result;
            tasks = JsonConvert.DeserializeObject<List<Task>>(response);

            // get all teams
            response = client.GetStringAsync("/api/Teams").Result;
            teams = JsonConvert.DeserializeObject<List<Team>>(response);

            // get all projects
            response = client.GetStringAsync("/api/Projects").Result;
            projects = JsonConvert.DeserializeObject<List<Project>>(response);
        }

        private void GetProjectStruct()
        {
            // general structure
            projectStruct = from project in projects
                            join team in teams on project.TeamId equals team.Id
                            join author in users on project.AuthorId equals author.Id
                            join task in tasks on project.Id equals task.ProjectId into tList
                            select new ProjectData
                            {
                                Id = project.Id,
                                Name = project.Name,
                                Description = project.Description,
                                CreatedAt = project.CreatedAt,
                                Deadline = project.Deadline,
                                Team = new Team()
                                {
                                    Id = team.Id,
                                    Name = team.Name,
                                    CreatedAt = team.CreatedAt
                                },
                                Author = new UserData() { Id = author.Id, FirstName = author.FirstName, LastName = author.LastName },
                                Tasks = (from t in tList
                                         join performer in users on t.PerformerId equals performer.Id
                                         join teamPerf in teams on performer.TeamId equals teamPerf.Id into tJoin
                                         from tj in tJoin.DefaultIfEmpty()
                                         select new TaskData
                                         {
                                             Id = t.Id,
                                             Name = t.Name,
                                             Description = t.Description,
                                             CreatedAt = t.CreatedAt,
                                             FinishedAt = t.FinishedAt,
                                             State = t.State,
                                             Performer = new UserData()
                                             {
                                                 Id = performer.Id,
                                                 FirstName = performer.FirstName,
                                                 Team = tj != null ? new Team() { Id = tj.Id, Name = tj.Name } : null,
                                                 Birthday = performer.Birthday,
                                                 RegisteredAt = performer.RegisteredAt
                                             }
                                         }).ToList()
                            };
        }

        public List<(ProjectData project, TaskData longDescriptionTask, TaskData shortNameTask, int usersCount)> Task7_GetProjectShortInfo()
        {
            var result = projectStruct
                 .Select(p => (project: p,
                             longDescriptionTask: p.Tasks.OrderBy(t => t.Description.Length).LastOrDefault(),
                             shortNameTask: p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                             usersCount: (p.Description.Length > 20 || p.Tasks.Count < 3) ?
                             projectStruct
                             .Where(pr => pr.Team.Id == p.Team.Id)
                             .Select(pr => pr.Team.Id).Distinct()
                             .GroupJoin(projectStruct
                                     .SelectMany(pr => pr.Tasks)
                                     .GroupBy(t => t.Id)
                                     .Select(tg => tg.FirstOrDefault())
                                     .Select(t => t.Performer)
                                     .GroupBy(u => u.Id)
                                     .Select(ug => ug.FirstOrDefault()),
                                                             t => t,
                                                             u => u.Team?.Id,
                                                             (t, us) => us.Count()).Sum() : 0)).ToList();
            return result;
        }




        public UserInfo Task6_GetUserLastProjectInfo(int userId)
        {
            UserData user11 = projectStruct.Select(p => p.Author).FirstOrDefault(u => u.Id == userId) ??
                projectStruct.SelectMany(p => p.Tasks.Where(t => t.Performer.Id == userId))?.Select(t => t.Performer).FirstOrDefault();

            ProjectData lastProject = projectStruct.Where(p => p.Author.Id == userId)?.OrderBy(p => p.CreatedAt).LastOrDefault();

            int tasksLastProject = lastProject?.Tasks.Count ?? 0;
            int notComletedTasks = projectStruct.SelectMany(p => p.Tasks.Where(t => t.Performer.Id == userId &&
            t.State != 2)).Count();
            TaskData maxTask = projectStruct.SelectMany(p => p.Tasks.Where(t => t.Performer.Id == userId))
                .OrderBy(t => t.FinishedAt - t.CreatedAt).LastOrDefault();

            UserInfo result = new UserInfo()
            {
                User = user11,
                LastProject = lastProject,
                TasksLastProject = tasksLastProject,
                NotComletedTasks = notComletedTasks,
                MaxTask = maxTask
            };
            return result;
        }

        public List<(int id, string name, List<UserData> members)> Task4_GetAgeLimitTeams()
        {
            List<(int id, string name, List<UserData> members)> result = projectStruct
                .SelectMany(p => p.Tasks)
                .Select(t => t.Performer)
                .GroupBy(u => u.Id)
                .Select(u => u.FirstOrDefault())
                .Where(u => u.Birthday.Year < DateTime.Now.Year - 10 && u.Team != null)
                .OrderByDescending(u => u.RegisteredAt)
                .GroupBy(u => u.Team.Id)
                .Join(projectStruct
                .GroupBy(p => p.Team.Id)
                .Select(p => p.FirstOrDefault().Team), uGr => uGr.Key, t => t.Id, (uGr, t) => (uGr.Key, t.Name, uGr.ToList()))
                .ToList();
            return result;
        }

        public Dictionary<ProjectData, int> Task1_GetProjectUserTasksCount(int id)
        {
            return projectStruct.Where(p => p.Author.Id == id).ToDictionary(e => e, e => e.Tasks.Count); // number of tasks in users(id) project
        }

        public IEnumerable<TaskData> Task2_GetUserTasks(int id)
        {
            return projectStruct.SelectMany(p => p.Tasks.Where(t => t.Performer.Id == id && t.Name.Length < 45));
        }

        public IEnumerable<(int id, string name)> Task3_GetFinishedTasksForUser(int id)
        {
            return projectStruct.SelectMany(p => p.Tasks.Where(t => t.Performer.Id == id && t.FinishedAt.Year == DateTime.Now.Year && t.State == 2))
                .Select(t => (t.Id, t.Name)).ToList();
        }

        public IEnumerable<(UserData user, List<TaskData> tasks)> Task5_GetSortedUsers()
        {
            IEnumerable<(UserData user, List<TaskData> tasks)> result = projectStruct.SelectMany(p => p.Tasks)
                    .GroupBy(t => t.Id).Select(tg => tg.FirstOrDefault())
                    .GroupBy(t => t.Performer.Id,
                            t => t,
                            (uId, tg) => (user: tg.FirstOrDefault().Performer, tasks: tg.OrderByDescending(t => t.Name.Length).ToList()))
                    .OrderBy(ls => ls.user.FirstName);

            return result;
        }


        public void Dispose()
        {
            client?.Dispose();
        }
    }
}
