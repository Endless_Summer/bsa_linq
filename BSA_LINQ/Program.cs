﻿using BSA_LINQ.Services;
using System;

namespace BSA_LINQ
{

    class Program
    {
        static void Main(string[] args)
        {
            LinqService linqService = null;

            try
            {
                linqService = new LinqService();
                Menu.Start(linqService);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message}");
            }
            finally
            {
                linqService?.Dispose();
            }
        }
    }

}
